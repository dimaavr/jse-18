package ru.tsc.avramenko.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(String Name);

}
